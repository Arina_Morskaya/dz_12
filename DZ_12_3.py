import sys
import time


class MyContextManager(object):
    def __init__(self):
        self.time = None
        self.start = None

    def __call__(self, func):
        def wrapper():
            exc_type, exc_val, exc_tb = (None,) * 3
            self.__enter__()
            self.start = time.time()
            func()
            self.__exit__(exc_type, exc_val, exc_tb)
            print('Decorator - ', time.time() - self.start)
        return wrapper

    def __enter__(self):
        self.time = time.time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.start is None:
            print('ContextManager - ', time.time() - self.time)


@MyContextManager()
def my_exception():
    a = [x * x for x in range(300000)]
    try:
        print(a[400000])
    except IndexError:
        print('exception time - ', end='')


@MyContextManager()
def conditional_op():
    b = [x * x for x in range(300000)]
    if len(b) > 400000:
        print(b[400000])
    else:
        print('conditional operator - ', end='')


with MyContextManager():
    c = [x * x for x in range(300000)]
    try:
        print(c[400000])
    except IndexError:
        print('exception time - ', end='')


with MyContextManager():
    d = [x * x for x in range(300000)]
    if len(d) > 400000:
        print(d[400000])
    else:
        print('conditional operator - ', end='')

my_exception()
conditional_op()
