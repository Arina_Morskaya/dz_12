import datetime


class MyException(Exception):
    def __init__(self, msg, login_name):
        self.name = login_name
        self.msg = msg

    def write_file(self):
        with open('D:/Error.txt', 'a') as error:
            error.write('{}: login attempt user: {} msg: {}\n'.format
                        (datetime.datetime.now(), self.name, self.msg))


def login(name):
    if name.isalpha():
        pass
    else:
        raise MyException('Invalid data format', name)


try:
    login('12345')
except MyException as e:
    e.write_file()
