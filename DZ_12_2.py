from contextlib import contextmanager
import time


def fib(n):
    a, b = 0, 1
    for _ in range(n + 1):
        yield a
        a, b = b, a + b


@contextmanager
def timer():
    t = time.time()
    try:
        yield
    finally:
        print('Fibonacci calculation time: ', time.time() - t)


with timer():
    print(list(fib(7000)))


